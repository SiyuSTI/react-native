import React, {Component} from 'react';
import { Text,View, Button } from 'react-native';
import Post from '../Post/Post';
import {StackNavigator} from 'react-navigation';
import ViewCar from './ViewCar';
import ViewPost from '../Post/ViewPost';

class Page2 extends Component {
  
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    return{
      headerRight: (
        <Button title="Post" color="#fff" onPress= {()=> {navigation.navigate('Post')}} />
 
      ),
    };
  }

  
    render () {
      return (
       <ViewCar {...this.props} />
      );
    }
}
const Stack=StackNavigator(
  {
    Home:{
      screen:Page2,
    },
    Post: {
      screen: Post,
    }, 
    viewPost: {
      screen: ViewPost,
    }
  },

  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }

);
export default Stack;
