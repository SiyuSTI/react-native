import React, {Component} from 'react';
import { Text,View, Button } from 'react-native';
import Post from '../Post/Post';
import {StackNavigator} from 'react-navigation';
import ViewSecondhand from './ViewSecondhand';
import ViewPost from '../Post/ViewPost';
class Page3 extends Component {
  
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    return{
      headerRight: (
        <Button title="Post" color="#fff" onPress= {()=> {navigation.navigate('Post')}} />
 
      ),
    };
  }

  
    render () {
      return ( 
       <ViewSecondhand {...this.props} />
      );
    }
}


const Stack=StackNavigator(
  {
    Home:{
      screen:Page3,
    },
    Post: {
      screen: Post,
    }, 
    viewPost: {
      screen: ViewPost,
    }
  },

  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }

);
export default Stack;
